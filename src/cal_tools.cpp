#include <cstdio>
#include <map>
#include "cal_tools.h"
#include "hit.h"
#include "id.h"

void get_cal(FILE* f, std::map<daq_id_t, cal_info_t>& calMap) {
  while (!feof(f)) {
    daq_id_t id;
    cal_info_t cal;
    // Read line
    fscanf(f, "%ld\t%ld\t%ld\t%ld\t%lf\t%lf", &id.sourceid, &id.crateid, &id.slotid, &id.chanid, &cal.offset, &cal.slope);

    if(feof(f)) {
      break;
    }
    // Process data
    if (calMap.count(id) != 0) {
      fprintf(stderr, "daq_id (%ld,%ld,%ld,%ld) already has a calibration.\n", id.sourceid, id.crateid, id.slotid, id.chanid);
    }
    calMap[id] = cal;
  }
}

void calibrate(hits_t& hits, const std::map<daq_id_t, cal_info_t>& calMap) {
  uint64_t size = *hits.get_len_ptr();
  uint64_t*  so = hits.get_sourceid_ptr();
  uint64_t*  cr = hits.get_crateid_ptr();
  uint64_t*  sl = hits.get_slotid_ptr();
  uint64_t*  ch = hits.get_chanid_ptr();
  //uint64_t*  di = hits.get_detid_ptr();
  //uint64_t*  dc = hits.get_detch_ptr();
  //uint64_t*  rv = hits.get_rawval_ptr();
  uint64_t*   v = hits.get_value_ptr();
  double*    en = hits.get_energy_ptr();

  for (size_t j = 0; j < size; ++j) {
    daq_id_t id(so[j], cr[j], sl[j], ch[j]);

    if (calMap.count(id) != 0) {
      cal_info_t cal(calMap.at(id));

      en[j] = cal.offset + cal.slope * v[j];
    } else {
      en[j] = v[j];
    }
  }
}
