#ifndef _DET_TOOLS_H_
#define _DET_TOOLS_H_
#include <cstdio>
#include <map>
#include <vector>
#include "detectors/detector_t.h"
#include "hit.h"
#include "id.h"

void get_allDets(FILE* f, std::vector<detector_t*>& allDets, std::map<daq_id_t, det_id_t>& chanMap);
void make_dets(hits_t& hits, const std::vector<detector_t*>& allDets, const std::map<daq_id_t, det_id_t>& chanMap);

#endif
