#ifndef _CAL_TOOLS_H_
#define _CAL_TOOLS_H_
#include <cstdio>
#include <map>
#include "cal_info_t.h"
#include "hit.h"
#include "id.h"

void get_cal(FILE* f, std::map<daq_id_t, cal_info_t>& calMap);
void calibrate(hits_t& hits, const std::map<daq_id_t, cal_info_t>& calMap);

#endif
