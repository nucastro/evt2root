#include <cstdio>
#include <cstring>
#include <map>
#include <TNamed.h>
#include <vector>
#include "detectors/detectors.h"
#include "detectors/detector_t.h"
#include "det_tools.h"
#include "hit.h"
#include "id.h"

void get_allDets(FILE* f, std::vector<detector_t*>& allDets, std::map<daq_id_t, det_id_t>& chanMap) {
  TString ts;

  while (!feof(f)) {
    char type[100];
    fscanf(f, "%s", type);

    if(feof(f)) {
      break;
    }

    if (strcmp(type, "BB10_F") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new BB10_F_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "BB15_B") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new BB15_B_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "BB15_F") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new BB15_F_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "HABANERO") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new HABANERO_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "HAGRID") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new HAGRID_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "PSIC_E") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new PSIC_E_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "PSIC_XY") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new PSIC_XY_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "QQQ3_B") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new QQQ3_B_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "QQQ3_F") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new QQQ3_F_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "QQQ5_B") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new QQQ5_B_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "QQQ5_F") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new QQQ5_F_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "YY1_F") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new YY1_F_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else if (strcmp(type, "TAC") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(f, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new TAC_t(name, id));
      ts += TString::Format("%s\t%s\t%ld\t%ld\t%ld\t%ld\n", type, name, id.sourceid, id.crateid, id.slotid, id.chanid);
    } else {
      fprintf(stderr, "Unknown detector type: \"%s\"\n", type);
      while (getc(f) != '\n') {}
    }
  }

  TNamed* det_info = new TNamed("det_info", ts.Data());
  det_info->Write(0, TObject::kOverwrite);

  for (size_t i = 0; i < allDets.size(); ++i) {
    for (size_t j = 0; j < allDets[i]->size(); ++j) {
      daq_id_t id = allDets[i]->det_to_daq(j);
      det_id_t det(i+1, j);

      if (chanMap.count(id) != 0) {
        fprintf(stderr, "daq_id (%ld,%ld,%ld,%ld) already used.\n", id.sourceid, id.crateid, id.slotid, id.chanid);
      }
      chanMap[id] = det;
    }
  }
}


void make_dets(hits_t& hits, const std::vector<detector_t*>& allDets, const std::map<daq_id_t, det_id_t>& chanMap) {
  uint64_t size = *hits.get_len_ptr();
  uint64_t*  so = hits.get_sourceid_ptr();
  uint64_t*  cr = hits.get_crateid_ptr();
  uint64_t*  sl = hits.get_slotid_ptr();
  uint64_t*  ch = hits.get_chanid_ptr();
  uint64_t*  di = hits.get_detid_ptr();
  uint64_t*  dc = hits.get_detch_ptr();
  uint64_t*  rv = hits.get_rawval_ptr();
  uint64_t*   v = hits.get_value_ptr();
  double*    en = hits.get_energy_ptr();

  for (size_t j = 0; j < size; ++j) {
    daq_id_t id1(so[j], cr[j], sl[j], ch[j]);

    if (chanMap.count(id1) != 0) {
      det_id_t id2(chanMap.at(id1));

      di[j] = id2.detid;
      dc[j] = id2.detch;
      v[j] = allDets[id2.detid-1]->val_corr(id1, rv[j]);
    } else {
      di[j] = 0;
      dc[j] = 0;
      v[j] = rv[j];
    }
    en[j] = v[j];
  }
}
