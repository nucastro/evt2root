#ifndef _HAGRID_H_
#define _HAGRID_H_

#include <stdint.h>
#include <string>
#include "../id.h"
#include "detector_t.h"

class HAGRID_t: public detector_t {
protected:
  daq_id_t startId;
public:
  HAGRID_t(std::string n, daq_id_t id);
public:
  virtual bool contains_daq(daq_id_t id);
  virtual uint64_t daq_to_det(daq_id_t id);
  virtual daq_id_t det_to_daq(uint64_t ch);
};
#endif
