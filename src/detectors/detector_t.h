#ifndef _DETECTOR_T_H_
#define _DETECTOR_T_H_

#include <stdint.h>
#include <string>
#include <vector>
#include "../id.h"

class detector_t {
protected:
  std::string name;
  size_t numChans;
public:
  detector_t(size_t c);
public:
  std::string get_name();
  size_t size();
public:
  virtual uint64_t val_corr(daq_id_t id, uint64_t value);
public:
  virtual bool contains_daq(daq_id_t id) = 0;
  virtual uint64_t daq_to_det(daq_id_t id) = 0;
  virtual daq_id_t det_to_daq(uint64_t ch) = 0;
};
#endif
