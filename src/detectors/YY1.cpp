#include <stdint.h>
#include <string>
#include "../id.h"
#include "YY1.h"

YY1_F_t::YY1_F_t(std::string n, daq_id_t id): detector_t(16) {
  name = n;
  startId = id;
}

uint64_t YY1_F_t::val_corr(daq_id_t id __attribute__((unused)), uint64_t value) {
  return 16383-value;
}

bool YY1_F_t::contains_daq(daq_id_t id) {
  if (id.sourceid == startId.sourceid) {
    if (id.crateid == startId.crateid) {
      if (id.slotid == startId.slotid) {
        return true;
      }
    }
  }
  return false;
}

uint64_t YY1_F_t::daq_to_det(daq_id_t id) {
  if (!contains_daq(id)) {
    return -1;
  }
  return id.chanid;
}

daq_id_t YY1_F_t::det_to_daq(uint64_t ch) {
  daq_id_t id = startId;
  id.chanid += ch;
  return id;
}
