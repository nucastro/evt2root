#ifndef _DETECTORS_H_
#define _DETECTORS_H_

#include "BB10.h"
#include "BB15.h"
#include "detector_t.h"
#include "HABANERO.h"
#include "HAGRID.h"
#include "QQQ3.h"
#include "QQQ5.h"
#include "PSIC.h"
#include "TAC.h"
#include "YY1.h"

#endif
