#include <stdint.h>
#include <string>
#include "../id.h"
#include "HABANERO.h"

HABANERO_t::HABANERO_t(std::string n, daq_id_t id): detector_t(80) {
  name = n;
  startId = id;
}

bool HABANERO_t::contains_daq(daq_id_t id) {
  if (id.sourceid == startId.sourceid) {
    if (id.crateid == startId.crateid) {
      if ((id.slotid >= startId.slotid) && (id.slotid < startId.slotid + 5)) {
        return true;
      }
    }
  }
  return false;
}

uint64_t HABANERO_t::daq_to_det(daq_id_t id) {
  if (!contains_daq(id)) {
    return -1;
  }
  return 16*(id.slotid-startId.slotid) + id.chanid;
}

daq_id_t HABANERO_t::det_to_daq(uint64_t ch) {
  daq_id_t id = startId;
  id.slotid += ch/16;
  id.chanid += ch%16;
  return id;
}
