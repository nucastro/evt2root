#ifndef _QQQ3_H_
#define _QQQ3_H_

#include <stdint.h>
#include <string>
#include "../id.h"
#include "detector_t.h"

class QQQ3_F_t: public detector_t {
protected:
  daq_id_t startId;
public:
  QQQ3_F_t(std::string n, daq_id_t id);
public:
  virtual uint64_t val_corr(daq_id_t chan, uint64_t value);
public:
  virtual bool contains_daq(daq_id_t id);
  virtual uint64_t daq_to_det(daq_id_t id);
  virtual daq_id_t det_to_daq(uint64_t ch);
};

class QQQ3_B_t: public detector_t {
protected:
  daq_id_t startId;
public:
  QQQ3_B_t(std::string n, daq_id_t id);
public:
  virtual bool contains_daq(daq_id_t id);
  virtual uint64_t daq_to_det(daq_id_t id);
  virtual daq_id_t det_to_daq(uint64_t ch);
};
#endif
