#ifndef _HIT_H_
#define _HIT_H_

#include <cstddef>
#include <stdint.h>
#include <vector>

class single_hit_t {
public:
  uint64_t sourceid;
  uint64_t crateid;
  uint64_t slotid;
  uint64_t chanid;
  uint64_t detid;
  uint64_t detch;
  uint64_t rawval;
  uint64_t value;
  double   energy;
  double   time;
  std::vector<uint16_t> trace;
public:
  single_hit_t();
  single_hit_t(uint64_t so, uint64_t cr, uint64_t sl, uint64_t ch, uint64_t di, uint64_t dc, uint64_t rv, uint64_t v, double en, double t);
  single_hit_t(uint64_t so, uint64_t cr, uint64_t sl, uint64_t ch, uint64_t di, uint64_t dc, uint64_t rv, uint64_t v, double en, double t, const std::vector<uint16_t>& tr);
};

class hits_t {
protected:
  size_t maxMult;
  uint64_t len;
  std::vector<uint64_t> sourceid;
  std::vector<uint64_t> crateid;
  std::vector<uint64_t> slotid;
  std::vector<uint64_t> chanid;
  std::vector<uint64_t> detid;
  std::vector<uint64_t> detch;
  std::vector<uint64_t> rawval;
  std::vector<uint64_t> value;
  std::vector<double>   energy;
  std::vector<double>   time;
  std::vector<std::vector<uint16_t> > trace;
public:
  hits_t();
  hits_t(size_t s);
public:
  void set_hit(size_t i, single_hit_t h);
  single_hit_t get_hit(size_t i);
  void operator=(const hits_t& a);
  void Reset();
  void reserve(size_t s);
  void push_back(single_hit_t h);
public:
  size_t size();
  size_t get_maxMult();
  uint64_t* get_len_ptr();
  uint64_t* get_sourceid_ptr();
  uint64_t* get_crateid_ptr();
  uint64_t* get_slotid_ptr();
  uint64_t* get_chanid_ptr();
  uint64_t* get_detid_ptr();
  uint64_t* get_detch_ptr();
  uint64_t* get_rawval_ptr();
  uint64_t* get_value_ptr();
  double*   get_energy_ptr();
  double*   get_time_ptr();
  std::vector<uint16_t>* get_trace_ptr();
};
#endif
