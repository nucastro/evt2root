#ifndef _ID_H_
#define _ID_H_

#include <stdint.h>

class daq_id_t {
public:
  uint64_t sourceid;
  uint64_t slotid;
  uint64_t crateid;
  uint64_t chanid;
public:
  daq_id_t();
  daq_id_t(uint64_t so, uint64_t cr, uint64_t sl, uint64_t ch);
};

class det_id_t {
public:
  uint64_t detid;
  uint64_t detch;
public:
  det_id_t();
  det_id_t(uint64_t di, uint64_t dc);
};

bool operator< (const daq_id_t& a, const daq_id_t& b);
#endif
