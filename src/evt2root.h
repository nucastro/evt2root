#ifndef _EVT2ROOT_H_
#define _EVT2ROOT_H_
#include <map>
#include <stdint.h>

enum sourcetype_t {ASICS, DDAS};

extern std::map<uint64_t, sourcetype_t> SOURCES;
extern bool EVT_DEBUG;
extern bool IS_GLOM;
#endif
