#ifndef _EVENT_T_H_
#define _EVENT_T_H_

/* event_t
 *
 * This class represents an event from the ring buffer.
 * There are different types of ring items that an event can hold.
 * Each type of ring item has a specific value for the type variable.
 * Some of the types have the same structure, so they share a class.
 */
#include <stdint.h>
#include <vector>
#include "../hit.h"
#include "body_header_t.h"
#include "ring_items.h"

class event_t {
public:
  hits_t hits;
public:
  uint64_t size; // Number of bits of the event (inclusive)
  uint64_t type;
  body_header_t* bodyHeader;
  ring_item_t* ringItem;
public:
  event_t(const std::vector<uint8_t>& buf);
  ~event_t();
protected:
  void populate_hits();
  void populate_hits_from_asics(asics_payload_t* payload, uint64_t sourceID);
  void populate_hits_from_ddas(ddas_payload_t* payload, uint64_t sourceID);
};

#endif
