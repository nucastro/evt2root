#include <cstdio>
#include "event_utils.h"

uint64_t getBufWord(const std::vector<uint8_t>& buffer, size_t& idx, const size_t size, bool advance) {
  buffer_data_t d = {0};
  if (size > 8) { // only supports up to 8 bytes (doesn't advance idx)
    fprintf(stderr, "getBufWord called with size > 8.\n");
  } else {
    for (size_t i = 0; i < size; ++i) {
      d.bytes[i] = buffer[idx+i];
    }
    if (advance) {
      idx += size;
    }
  }
  return d.all;
}
