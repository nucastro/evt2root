#ifndef _EVENT_UTILS_H_
#define _EVENT_UTILS_H_

/* event_utils
 *
 * Here are some utilities for reading in an event buffer.
 */

#include <stdint.h>
#include <cstddef>
#include <vector>

/* buffer_data_t
 * 
 * This is a union. You can put a bunch of bytes together to form a uint64_t
 */
union buffer_data_t {
  uint64_t all;
  uint8_t bytes[8];
};

/* getBufWord
 *
 * This function reads from a vector of bytes (buffer) at a supplied index (idx)
 * for a certain number of bytes (size), and returns a uint64_t.
 * By default, idx is advanced past the read data, but that can by disabled.
 */
uint64_t getBufWord(const std::vector<uint8_t>& buffer, size_t& idx, const size_t size, const bool advance = true);

#endif
