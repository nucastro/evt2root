#include <stdint.h>
#include "id.h"

daq_id_t::daq_id_t() {
}

daq_id_t::daq_id_t(uint64_t so, uint64_t cr, uint64_t sl, uint64_t ch) {
  sourceid = so;
  crateid = cr;
  slotid = sl;
  chanid = ch;
}

det_id_t::det_id_t() {
}

det_id_t::det_id_t(uint64_t di, uint64_t dc) {
  detid = di;
  detch = dc;
}

bool operator<(const daq_id_t& a, const daq_id_t& b) {
  if (a.sourceid < b.sourceid) {
    return true;
  } else if (b.sourceid < a.sourceid) {
    return false;
  }

  if (a.crateid < b.crateid) {
    return true;
  } else if (b.crateid < a.crateid) {
    return false;
  }

  if (a.slotid < b.slotid) {
    return true;
  } else if (b.slotid < a.slotid) {
    return false;
  }

  if (a.chanid < b.chanid) {
    return true;
  } else if (b.chanid < a.chanid) {
    return false;
  }

  return false;
}
