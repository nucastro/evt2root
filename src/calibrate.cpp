#include <cstdio>
#include <map>
#include <string>
#include <TBranch.h>
#include <TFile.h>
#include <TTree.h>
#include <vector>
#include "cal_info_t.h"
#include "cal_tools.h"
#include "hit.h"
#include "id.h"

int main () {
  std::map<daq_id_t, cal_info_t> calMap;
  FILE* calfile;

  if ((calfile=fopen("calib.dat", "r")) == NULL) {
    fprintf(stderr,"ERROR: Can't open calibration file '%s'!\n", "calib.dat");
    exit(-2);
  }
  get_cal(calfile, calMap);

  TFile* file = new TFile("out.root", "update");
  TTree* tree = (TTree*)file->Get("tree");

  hits_t hits(1000);//FIXME: make adjustable

  tree->SetBranchAddress("size",     (ULong64_t*)hits.get_len_ptr());
  tree->SetBranchAddress("sourceid", (ULong64_t*)hits.get_sourceid_ptr());
  tree->SetBranchAddress("crateid",  (ULong64_t*)hits.get_crateid_ptr());
  tree->SetBranchAddress("slotid",   (ULong64_t*)hits.get_slotid_ptr());
  tree->SetBranchAddress("chanid",   (ULong64_t*)hits.get_chanid_ptr());
  tree->SetBranchAddress("value",    (ULong64_t*)hits.get_value_ptr());
 //FIXME setbranchaddress if branches already exist
  tree->Branch("energy", hits.get_energy_ptr(), "energy[size]/D");

  int nEntries = tree->GetEntries();
  for (int i = 0; i < nEntries; ++i) {
    printf("%5.2f%%\r", 100.*i/nEntries);
    tree->GetEntry(i);

    calibrate(hits, calMap);

    tree->Fill();
  }
  file->Write();
  file->Close();
}
