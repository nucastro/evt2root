#include <cstdio>
#include <map>
#include <string>
#include <TBranch.h>
#include <TFile.h>
#include <TNamed.h>
#include <TTree.h>
#include <vector>
#include "detectors/detectors.h"
#include "detectors/detector_t.h"
#include "det_tools.h"
#include "hit.h"
#include "id.h"

int main () {
  std::vector<detector_t*> allDets;
  std::map<daq_id_t, det_id_t> chanMap;
  FILE* detfile;



  if ((detfile=fopen("dets.cfg", "r")) == NULL) {
    fprintf(stderr,"ERROR: Can't open config file '%s'!\n", "dets.cfg");
    exit(-2);
  }
  get_allDets(detfile, allDets, chanMap);



  TFile* file = new TFile("out.root", "update");
  TTree* tree = (TTree*)file->Get("tree");

  hits_t hits(1000);//FIXME: make adjustable

  tree->SetBranchAddress("size",     (ULong64_t*)hits.get_len_ptr());
  tree->SetBranchAddress("sourceid", (ULong64_t*)hits.get_sourceid_ptr());
  tree->SetBranchAddress("crateid",  (ULong64_t*)hits.get_crateid_ptr());
  tree->SetBranchAddress("slotid",   (ULong64_t*)hits.get_slotid_ptr());
  tree->SetBranchAddress("chanid",   (ULong64_t*)hits.get_chanid_ptr());
  tree->SetBranchAddress("rawval",   (ULong64_t*)hits.get_rawval_ptr());
  //FIXME setbranchaddress if branches already exist
  tree->Branch("detid", hits.get_detid_ptr(), "detid[size]/L");
  tree->Branch("detch", hits.get_detch_ptr(), "detch[size]/l");
  tree->Branch("value", hits.get_value_ptr(), "value[size]/l");

  int nEntries = tree->GetEntries();
  for (int i = 0; i < nEntries; ++i) {
    printf("%5.2f%%\r", 100.*i/nEntries);
    tree->GetEntry(i);

    //make_dets(tree, hits, allDets, chanMap); //FIXME
    make_dets(hits, allDets, chanMap);
    tree->Fill();// FIXME: only fill branches?
  }



  file->Write();
  file->Close();
}
