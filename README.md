# evt2root
`evt2root` is a tool that converts from the [NSCL DAQ] ringbuffer format to a
[ROOT] file.

[NSCL DAQ]: http://docs.nscl.msu.edu/daq/index.php
[ROOT]: https://root.cern.ch/

## Installation
* Clone the repo: `git clone https://brownej@bitbucket.org/nucastro/evt2root.git`
* Change to the build directory: `cd evt2root/build`
* Compile the executables: `make`
* Install the executables to the `bin` directory: `make install`

## Usage
```
evt2root (-e evt_file | -b batch_file) [-o output_file] [-s sourceid_file] [-d detector_file] [-c calibration_file]
```

* `-e evt_file`: An nscldaq-11.1 ringbuffer `.evt` file.
* `-b batch_file`: A text file containing a list of nscldaq-11.1 ringbuffer `.evt` files, separated by newlines.
* `-o output_file`: A ROOT file, which will be created, or reacreated if it already exists [default: `out.root`].
* `-s sourceid_file`: A text file containing the sourceid configuration [default: `sources.cfg`]. If the file is not found, the program will exit.
* `-d detector_file`: A text file containing the detector configuration. If this flag is not supplied, the detector configuration is not performed.
* `-c calibration_file`: A text file containing the calibration information. If this flag is not supplied, the calibration is not performed.

### output\_file
The ouput ROOT file contains a `TTree` named `tree` with the following branches:

* `sourceid`: Part of the DAQ ID (`uint64_t`)
* `crateid`: Part of the DAQ ID (`uint64_t`)
* `slotid`: Part of the DAQ ID (`uint64_t`)
* `chanid`: Part of the DAQ ID (`uint64_t`)
* `detid`: Part of the detector ID, if a detector configuration is provided and the hit occured in a configured detector. Otherwise, it is 0. (`uint64_t`)
* `detch`: Part of the detector ID, if a detector configuration is provided and the hit occured in a configured detector. Otherwise, it is 0.(`uint64_t`)
* `rawval`: Energy channel, accoridng to the DAQ. (`uint64_t`)
* `value`: Energy channel corrected for polarity, if a detector configuration is provided and the hit occured in a configured detector. Otherwise, it is the same as `rawval`. (`uint64_t`)
* `energy`: Calibrated energy, if a calibration is provided. Otherwise, it is the same as `value`. (`double`)
* `time`: Time of the hit, according to the DAQ. (`double`)

It also contains a `TNamed` named `det_info` which contains the detector configuration and a `TNamed` named `file_info` which contains the `.evt` files that were used to create it.

### sourceid\_file
Format:
```
sourceid	source_type
```

The following source types are currently supported:

* `asics`
* `ddas`

### detector\_file
Format:
```
det_type	det_name	sourceid	crate_id	slotid	chanid
```

The following detector types are supported:

* `BB10_F`
* `BB15_B`
* `BB15_F`
* `HABANERO`
* `HAGRID`
* `PSIC_E`
* `PSIC_XY`
* `QQQ3_B`
* `QQQ3_F`
* `QQQ5_B`
* `QQQ5_F`
* `TAC`
* `YY1_F`

### calibration\_file
Format:
```
sourceid	crate_id	slotid	chanid	offset	slope
```

## Examples
### Example Invocations
```
evt2root -b ./config/runs.in
```
In this example, the `.evt` files found in `./config/runs.in` are converted
to a root tree, which is output to `./out.root` (the default).

`./sources.cfg` (the default) will be used as the sourceid configuration file.
If it is not found, the conversion will fail.

No detector configuration will be applied.
`detid` will be 0, `detch` will be 0, and `value` will be equal to `rawval`.

No energy calibration will be applied.
`energy` will be equal to `value`.

---

```
evt2root -e ~/experiment/run187/run-0187-00.evt
```
In this example, the data in `~/experiment/run187/run-0187-00.evt` is converted
to a root tree, which is output to `./out.root` (the default).

`./sources.cfg` (the default) will be used as the sourceid configuration file.
If it is not found, the conversion will fail.

No detector configuration will be applied.
`detid` will be 0, `detch` will be 0, and `value` will be equal to `rawval`.

No energy calibration will be applied.
`energy` will be equal to `value`.

---

```
evt2root -e ~/experiment/run187/run-0187-00.evt -o run187.root
```
In this example, the data in `~/experiment/run187/run-0187-00.evt` is converted
to a root tree, which is output to `./run187.root`.

`./sources.cfg` (the default) will be used as the sourceid configuration file.
If it is not found, the conversion will fail.

No detector configuration will be applied.
`detid` will be 0, `detch` will be 0, and `value` will be equal to `rawval`.

No energy calibration will be applied.
`energy` will be equal to `value`.

---

```
evt2root -e ~/experiment/run187/run-0187-00.evt -o run187.root -s ./config/sources.cfg
```
In this example, the data in `~/experiment/run187/run-0187-00.evt` is converted
to a root tree, which is output to `./run187.root`.

`./config/sources.cfg` will be used as the sourceid configuration file.
If it is not found, the conversion will fail.

No detector configuration will be applied.
`detid` will be 0, `detch` will be 0, and `value` will be equal to `rawval`.

No energy calibration will be applied.
`energy` will be equal to `value`.

---

```
evt2root -e ~/experiment/run187/run-0187-00.evt -d ./config/dets.cfg
```
In this example, the data in `~/experiment/run187/run-0187-00.evt` is converted
to a root tree, which is output to `./out.root` (the default).

`./sources.cfg` (the default) will be used as the sourceid configuration file.
If it is not found, the conversion will fail.

The detector configuration in `./config/dets.cfg` will be applied.

No energy calibration will be applied.
`energy` will be equal to `value`.

---

```
evt2root -e ~/experiment/run187/run-0187-00.evt -d ./config/dets.cfg -c ./config/calib.dat
```
In this example, the data in `~/experiment/run187/run-0187-00.evt` is converted
to a root tree, which is output to `./out.root` (the default).

`./sources.cfg` (the default) will be used as the sourceid configuration file.
If it is not found, the conversion will fail.

The detector configuration in `./config/dets.cfg` will be applied.

The energy calibration in `./config/calib.dat` will be applied.

---

```
evt2root -e ~/experiment/run187/run-0187-00.evt -c ./config/calib.dat
```
In this example, the data in `~/experiment/run187/run-0187-00.evt` is converted
to a root tree, which is output to `./out.root` (the default).

`./sources.cfg` (the default) will be used as the sourceid configuration file.
If it is not found, the conversion will fail.

No detector configuration will be applied.
`detid` will be 0, `detch` will be 0, and `value` will be equal to `rawval`.

The energy calibration in `./config/calib.dat` will be applied.

*Be careful!*
Since the detector calibration was not applied (meaning `value` has not been
corrected), the energy calibration might be wrong.

### Example Files
* [batch\_file](./config/runs.in)
* [sourceid\_file](./config/sources_JENSA.cfg)
* [detector\_file](./config/dets_e15232.cfg)
* [calibration\_file](./config/calib_e15232.dat)

## Other Tools
These are not yet ready

### make\_dets
### calibrate
